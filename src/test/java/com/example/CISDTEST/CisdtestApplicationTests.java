package com.example.CISDTEST;

import com.example.CISDTEST.service.IService;
import com.example.CISDTEST.service.PlusService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CisdtestApplicationTests {

	@Test
	void plusServiceTest() {
		PlusService plusService = new PlusService();

		int result1 = plusService.doSomething(2,3);

		Assertions.assertEquals(5, result1);

		int result2 = plusService.doSomething(-2,-3);
		Assertions.assertEquals(-5, result2);


		int result3 = plusService.doSomething(0,0);
		Assertions.assertEquals(0, result3);
	}

}
