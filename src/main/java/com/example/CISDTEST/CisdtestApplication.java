package com.example.CISDTEST;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CisdtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CisdtestApplication.class, args);
	}

}
