package com.example.CISDTEST;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Numbers {
    private int numberOne;
    private int numberTwo;
}
