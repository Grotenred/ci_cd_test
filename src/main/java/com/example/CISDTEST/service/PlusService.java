package com.example.CISDTEST.service;

import org.springframework.stereotype.Service;

@Service
public class PlusService implements IService{
    @Override
    public int doSomething(int a, int b) {
        return a + b;
    }
}
