package com.example.CISDTEST.controller;

import com.example.CISDTEST.Numbers;
import com.example.CISDTEST.service.IService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller("/home")
@RequiredArgsConstructor
public class MainController {

    private final IService iService;

    @GetMapping("/")
    public String getPage(Model model){
        Numbers numbers = new Numbers();
        model.addAttribute("numbers", numbers);
        return "index";
    }
    @PostMapping("/")
    public String doJob(@ModelAttribute("numbers") Numbers numbers, Model model){
        int result =  iService.doSomething(numbers.getNumberOne(), numbers.getNumberTwo());
        model.addAttribute("result", result);
        return "result";
    }
}
